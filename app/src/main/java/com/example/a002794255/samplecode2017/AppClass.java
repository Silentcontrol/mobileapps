package com.example.a002794255.samplecode2017;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.util.Log;
import android.widget.Toast;

import com.example.a002794255.samplecode2017.dataaccess.UserDataAccess;
import com.example.a002794255.samplecode2017.models.models.User;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * Created by 002794255 on 10/23/2017.
 */

public class AppClass extends Application {

    public static final String TAG = "AppClass";
    public String someGobalVariable = "Hello There!";
    public static ArrayList<User> users = new ArrayList();
    public static final String USERS_FILE = "users.dat";
    UserSQLiteHelper dbUserHelper;
    UserDataAccess da;
    @Override
    public void onCreate() {

        super.onCreate();
        /*
        users.add(new User(1, "Bob", "Bob@bob.com", User.Music.JAZZ, true));
        users.add(new User(2, "Sally", "Sally@bob.com", User.Music.RAP, true));
        users.add(new User(3, "Joe", "Joe@bob.com", User.Music.JAZZ, true));
        writeUsersToFile(users, USERS_FILE);

        Toast.makeText(this, users.toString(), Toast.LENGTH_LONG).show();
        users = readUsersFromFile(USERS_FILE);
        */
//        da = new UserDataAccess(dbUserHelper);
 //       users = da.getAllUser();

    }



    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }


    // Creates the user file
    public void writeUsersToFile(ArrayList<User> users, String filePath){
        try{
            FileOutputStream fos = openFileOutput(filePath, Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(users);
            oos.close();
            fos.close();
        }catch(FileNotFoundException e){
            Log.e(TAG, "File Not Found");
        }catch(IOException e){
            Log.e(TAG, "IO ERROR");
        }catch(Exception e){
            Log.e(TAG, "General Error");
        }
    }
    //Gives us access to the file that is created above.
    public ArrayList<User> readUsersFromFile(String filePath){
        try  {
            FileInputStream is = openFileInput(filePath);
            ObjectInputStream ois = new ObjectInputStream(is);
            users = (ArrayList<User>) ois.readObject();
            ois.close();
            is.close();
    }catch(FileNotFoundException e){
        Log.e(TAG, "File Not Found");
    }catch(IOException e){
        Log.e(TAG, "IO ERROR");
    }catch(Exception e){
        Log.e(TAG, "General Error");
    }
    return users;
    }

    public static User getUserById(long id){
        for(User u : users){
            if(id == u.getId()){
                return u;
            }
        }
        return null;
    }

}
