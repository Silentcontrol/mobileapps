package com.example.a002794255.samplecode2017;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;

public class ImageSpinnerActivity extends AppCompatActivity {

    public static final String  TAG = "ImageSpinnerActivity";

    Spinner spinner;
    ImageView imageView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_spinner);
        imageView = (ImageView)findViewById(R.id.imageView2);
        spinner = (Spinner)findViewById(R.id.spinner);

       spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
           @Override
           public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               String seletecedItem = spinner.getSelectedItem().toString();
               changeImage(seletecedItem);
               Log.d(TAG, "Just picked a new image!!!!");
           }

           @Override
           public void onNothingSelected(AdapterView<?> parent) {

           }
       });


       /* spinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = spinner.getSelectedItem().toString();
                changeImage(selectedItem);
            }
        });
*/

    }

    private void changeImage(String selected){
        int resourceid = 0;

        switch(selected){
            case "desert":
                resourceid = R.drawable.desert;
                break;
            case "jellyfish":
                resourceid = R.drawable.jellyfish;
                break;
            case "koala":
                resourceid = R.drawable.koala;
                break;
            case "lighthouse":
                resourceid = R.drawable.lighthouse;
                break;
            case "penguins":
                resourceid = R.drawable.penguins;
                break;
            case "tulips":
                resourceid = R.drawable.tulips;
                break;
        }
        imageView.setImageResource(resourceid);
    }
}
