package com.example.a002794255.samplecode2017;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.os.Bundle;
import android.widget.Toast;

/**
 * Created by 002794255 on 9/18/2017.
 */

public class TempMainActivity extends AppCompatActivity{

    Button btnImgSpinner;
    Button btnIntentSenders;
    Button btnUserDetails;
    Button btnadapter;
    Button btnUserList;
    Button btnLifeCycle;
    Button btnCoder;
    Button btnhttpRequest;
    Button btnSQLite;

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int ButtonResourceId = view.getId();
            switch(ButtonResourceId){
                case R.id.btnImage_Spinner:
                    startActivity(new Intent(TempMainActivity.this, ImageSpinnerActivity.class));
                    break;
                case R.id.btnIntent_Sender:
                    startActivity(new Intent(TempMainActivity.this, IntentSenderActivity.class));
                    break;
                case R.id.btnUser_Details:
                    startActivity(new Intent(TempMainActivity.this, UserDetailsActivity.class));
                    break;
                case R.id.btnAdapter:
                    startActivity(new Intent (TempMainActivity.this, Adapters.class));
                    break;
                case R.id.btnUserList:
                    startActivity(new Intent (TempMainActivity.this, UserListActivity.class));
                    break;
                case R.id.btnLifeCycle:
                    startActivity(new Intent (TempMainActivity.this, LifeCycleActivity.class));
                    break;
                case R.id.btnCoder:
                    startActivity(new Intent( TempMainActivity.this, CoderActivity.class));
                    break;
                case R.id.btnhttpRequest:
                    startActivity(new Intent(TempMainActivity.this, WebServiceActivtiy.class));
                    break;
                case R.id.btnSQLite:
                    startActivity(new Intent(TempMainActivity.this, SQLiteActivity.class));
                    break;
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temp_main);
        //Sets the button above to a specific button
        btnImgSpinner = (Button)findViewById(R.id.btnImage_Spinner);
        btnImgSpinner.setOnClickListener(listener);

        btnIntentSenders = (Button)findViewById(R.id.btnIntent_Sender);
        btnIntentSenders.setOnClickListener(listener);

        btnUserDetails = (Button)findViewById(R.id.btnUser_Details);
        btnUserDetails.setOnClickListener(listener);

        btnadapter = (Button)findViewById(R.id.btnAdapter);
        btnadapter.setOnClickListener(listener);

        btnUserList = (Button)findViewById(R.id.btnUserList);
        btnUserList.setOnClickListener(listener);

        btnLifeCycle = (Button)findViewById(R.id.btnLifeCycle);
        btnLifeCycle.setOnClickListener(listener);

        btnCoder = (Button)findViewById(R.id.btnCoder);
        btnCoder.setOnClickListener(listener);

        btnhttpRequest = (Button)findViewById(R.id.btnhttpRequest);
        btnhttpRequest.setOnClickListener(listener);

        btnSQLite = (Button)findViewById(R.id.btnSQLite);
        btnSQLite.setOnClickListener(listener);
    }
}


