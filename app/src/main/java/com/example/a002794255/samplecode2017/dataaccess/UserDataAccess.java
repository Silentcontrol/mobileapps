package com.example.a002794255.samplecode2017.dataaccess;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.example.a002794255.samplecode2017.UserSQLiteHelper;
import com.example.a002794255.samplecode2017.models.models.User;
import java.util.ArrayList;


/**
 * Created by 002794255 on 11/15/2017.
 */

public class UserDataAccess {
    public static final String TAG = "UserDataAccess";

    UserSQLiteHelper dbUserHelper;
    SQLiteDatabase database;

    public static final String TABLE_NAME = "users";
    public static final String COLUMN_USER_ID = "_id";
    public static final String COLUMN_USER_NAME = "user_name";
    public static final String COLUMN_USER_EMAIL = "user_email";
    public static final String COLUMN_FAVORITE_MUSIC = "fav_music";
    public static final String COLUMN_USER_ACTIVE = "user_active";

    public static final String TABLE_CREATE =
            String.format("create table %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s TEXT, %s TEXT, %s TEXT, %s INTEGER)",
                    TABLE_NAME,
                    COLUMN_USER_ID,
                    COLUMN_USER_NAME,
                    COLUMN_USER_EMAIL,
                    COLUMN_FAVORITE_MUSIC,
                    COLUMN_USER_ACTIVE

            );

    public UserDataAccess(UserSQLiteHelper dbUserHelper){
        this.dbUserHelper = dbUserHelper;
        this.database = this.dbUserHelper.getWritableDatabase();
    }

    public User insertUser(User u){
        ContentValues values = new ContentValues();
        values.put(COLUMN_USER_NAME, u.getFirstName());
        values.put(COLUMN_USER_EMAIL, u.getEmail());
        values.put(COLUMN_FAVORITE_MUSIC, u.getFavoriteMusic().toString());
        values.put(COLUMN_USER_ACTIVE, u.isActive());
        long insertId = database.insert(TABLE_NAME, null, values);
        u.setId(insertId);
        return u;
    }

    public boolean returnbool (int i) {
     if (i == 0){
         return true;
     }else{
         return false;
     }

    }

    public ArrayList<User> getAllUser(){
        ArrayList<User> Users = new ArrayList<>();
        String query = String.format("SELECT %s, %s, %s, %s, %s FROM %s",
                COLUMN_USER_ID,
                COLUMN_USER_NAME,
                COLUMN_USER_EMAIL,
                COLUMN_FAVORITE_MUSIC,
                COLUMN_USER_ACTIVE,
                TABLE_NAME);
        Cursor c = database.rawQuery(query, null);
        c.moveToFirst();
        while(!c.isAfterLast()) {
            User.Music um = null;
            String str = c.getString(3);
            if (str.equals("JAZZ")) {
                um = User.Music.JAZZ;
            } else if (str.equals("RAP")) {
                um = User.Music.RAP;
            } else if (str.equals("COUNTRY")) {
                um = User.Music.COUNTRY;
            }
            User u = new User();
            u.setId(c.getLong(0));
            u.setFirstName(c.getString(1));
            u.setEmail(c.getString(2));
            u.setFavoriteMusic(um);
            u.setActive(returnbool(c.getInt(4)));
            Users.add(u);
            c.moveToNext();
        }
        c.close();
        return Users;
    }



    public User updateUser(User u){
        ContentValues values = new ContentValues();
        values.put(COLUMN_USER_ID, u.getId());
        values.put(COLUMN_USER_NAME, u.getFirstName());
        values.put(COLUMN_USER_EMAIL, u.getEmail());
        values.put(COLUMN_FAVORITE_MUSIC, u.getFavoriteMusic().toString());
        values.put(COLUMN_USER_ACTIVE, u.isActive());

        int rowsUpdated = database.update(TABLE_NAME, values, "_id = " + u.getId(), null);
        return u;
    }

    public int deleteUser(long id){
        int rowsDeleted = database.delete(TABLE_NAME, COLUMN_USER_ID + " = " + id, null);
        // the above method returns the number of row that were deleted
        return rowsDeleted;
    }




}

