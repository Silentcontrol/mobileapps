package com.example.a002794255.samplecode2017;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.niallkader.Coder;

public class CoderActivity extends AppCompatActivity {
    EditText txtmessage;
    Button   btnEncrypt;
    Button   btnDecrypt;
    Coder coder = new Coder();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coder);

        txtmessage = (EditText) findViewById(R.id.txtMessage);
        btnEncrypt = (Button)findViewById(R.id.btnEncrypt);
        btnDecrypt = (Button)findViewById(R.id.btnDecrypt);

        btnEncrypt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = txtmessage.getText().toString();
                String encryptedMsg = coder.encode(msg);
                txtmessage.setText(encryptedMsg);
            }
        });
        btnDecrypt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = txtmessage.getText().toString();
                String decryptedMsg = coder.decode(msg);
                txtmessage.setText(decryptedMsg);
            }
        });
    }
}
