package com.example.a002794255.samplecode2017;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;


import com.example.a002794255.samplecode2017.dataaccess.UserDataAccess;
import com.example.a002794255.samplecode2017.models.models.User;

import java.util.ArrayList;



public class UserListActivity extends AppCompatActivity {
    UserSQLiteHelper dbUserHelper;
    UserDataAccess da;
    AppClass app;
    ListView userListView;
    ArrayList<User> users;
    ArrayAdapter<User> adapter;
    Button btnAddUser;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);

        app = (AppClass) getApplication();
        userListView = (ListView) findViewById(R.id.userListView);
        dbUserHelper = new UserSQLiteHelper(this);
        da = new UserDataAccess(dbUserHelper);
        app.users = da.getAllUser();

        adapter = new ArrayAdapter<User>(UserListActivity.this, android.R.layout.simple_list_item_1, users);
        btnAddUser = (Button)findViewById(R.id.btnAddUser);
        btnAddUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(UserListActivity.this, UserDetailsActivity.class));

            }
        });

        ArrayAdapter<User> adapter = new ArrayAdapter<User>(this, android.R.layout.simple_list_item_1, app.users);
        userListView.setAdapter(adapter);
        //listening for any click activity.
        userListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            //This represents the list inside the adapter
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                User selectedUser = app.users.get(position);
                Intent i = new Intent(UserListActivity.this, UserDetailsActivity.class);
                i.putExtra(UserDetailsActivity.USER_ID_EXTRA, selectedUser.getId());
                startActivity(i);

            }
        });

    }
}
