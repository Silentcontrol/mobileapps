package com.example.a002794255.samplecode2017;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

public class WebServiceActivtiy extends AppCompatActivity {
    public static final String WEB_SERVICE_SERVICE_URL = "http://wtc-web.com/users-web-service/users/";
    Button btnGetAllUsers;
    EditText txtResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_service);

        btnGetAllUsers = (Button)findViewById(R.id.btnGetAllUsers);
        txtResponse = (EditText)findViewById(R.id.txtResponse);

        btnGetAllUsers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isNetworkAvailable()){
                    doHttpRequest(WEB_SERVICE_SERVICE_URL);
                }else{
                    Toast.makeText(WebServiceActivtiy.this, "No Network", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private boolean isNetworkAvailable(){
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null && ni.isConnected();
    }

    private void doHttpRequest(String url){
        RequestQueue queue = Volley.newRequestQueue(this);

        StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        txtResponse.setText(response);
                    }
                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error){
                        Toast.makeText(WebServiceActivtiy.this, "HTTP Error", Toast.LENGTH_LONG).show();
                    }
                }
        );

        queue.add(stringRequest);
    }

}
