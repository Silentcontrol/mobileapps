package com.example.a002794255.samplecode2017;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class IntentSenderActivity extends AppCompatActivity {

    Button btnSend;
    EditText txtMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intent_sender);
        // Finds the values based on IDS without the views.
        btnSend = (Button)findViewById(R.id.btnSend);
        txtMessage = (EditText)findViewById(R.id.txtMessage);
        //Onclick on the button the page goes to a different view
        //Taking String to convert to correct Data type.
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              // This will send you to Google.com
                /*
                String url = "http://www.google.com";
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(url));
                startActivity(intent);
                */
                // This will send a the message inside txtMessage
                String messageText = txtMessage.getText().toString();
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                //Extracts the data and exports it into the intent then send it to messageText
                //Static varaibles
                //By changing the genaric key (Intent.EXTRA_TEXT) to IntentReceiverActiviy default Apps will not be able to receive the data.
                intent.putExtra(IntentReceiverActivity.MY_TEXT_DATA, messageText);

                //This will force the chooser to come up
                String chooserTitle = "Which App would like to handle this intent?";
                Intent choosenIntent = Intent.createChooser(intent, chooserTitle);
                startActivity(choosenIntent);
            }
        });
    }
}
