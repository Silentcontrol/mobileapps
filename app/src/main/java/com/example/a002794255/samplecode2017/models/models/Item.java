package com.example.a002794255.samplecode2017.models.models;

/**
 * Created by 002794255 on 11/13/2017.
 */

public class Item {
    private long id;
    private String name;

    public Item(long id, String name){

        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;

    }

    public void setName(String name) {

        this.name = name;
    }
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }




    @Override
    public String toString(){
        return this.name + " (" + this.id + ")";
    }
}

