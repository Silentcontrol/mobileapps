package com.example.a002794255.samplecode2017;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


import com.example.a002794255.samplecode2017.dataaccess.UserDataAccess;

/**
 * Created by 002794255 on 11/15/2017.
 */

public class UserSQLiteHelper extends SQLiteOpenHelper {

    public static final String TAG = "UserDataBase";
    public static final String USER_DATABASE = "users.db";
    public static final int DATABASE_VERSION = 1;

    public UserSQLiteHelper(Context context) {
        super(context, USER_DATABASE, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = UserDataAccess.TABLE_CREATE;
        db.execSQL(sql);
        Log.d(TAG, "Creating DB: " + sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
    }
}
