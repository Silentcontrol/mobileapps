package com.example.a002794255.samplecode2017;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.a002794255.samplecode2017.R;
import com.example.a002794255.samplecode2017.dataaccess.ItemDataAccess;
import com.example.a002794255.samplecode2017.models.models.Item;

import java.util.ArrayList;
import java.util.List;

public class SQLiteActivity extends AppCompatActivity {

    public static final String TAG = "SQLiteActivity";
    MySQLiteHelper dbHelper;
    ItemDataAccess da;
    ListView ItemslstViews;
    ArrayList<Item> items;
    Button btnAddItem;
    EditText txtItem;
    ArrayAdapter<Item> adapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sqlite);

        ItemslstViews = (ListView)findViewById(R.id.ItemslstView);
        btnAddItem = (Button)findViewById(R.id.btnAddItem);
        txtItem = (EditText)findViewById(R.id.txtItem);

        dbHelper = new MySQLiteHelper(this);
        da = new ItemDataAccess(dbHelper);
        items = da.getAllItems();
        adapter = new ArrayAdapter<Item>(this, android.R.layout.simple_list_item_1, items);
                ItemslstViews.setAdapter(adapter);

        btnAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String str = txtItem.getText().toString();
                if (!str.isEmpty()){
                    da.insertItem(new Item(0, str));
                    refreshItems();
                    txtItem.setText("");
                }else {
                    Toast.makeText(SQLiteActivity.this, "Please enter an item name", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
    public void refreshItems(){
        adapter.clear();
        items = da.getAllItems();
        adapter.addAll(items);
        adapter.notifyDataSetChanged();
    }
}
