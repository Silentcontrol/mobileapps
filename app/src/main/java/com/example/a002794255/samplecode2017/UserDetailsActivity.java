package com.example.a002794255.samplecode2017;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;


import com.example.a002794255.samplecode2017.dataaccess.UserDataAccess;
import com.example.a002794255.samplecode2017.models.models.User;


import java.io.Serializable;




public class UserDetailsActivity extends AppCompatActivity implements Serializable {
    public static final String USER_ID_EXTRA = "userId";
   public static String fileName = "SavedDataForm.ser";
    AppClass app;
    User user;
    RadioGroup Music;
    CheckBox chkActive;
    EditText EditTxtEmail;
    EditText EditTxtName;
    Button btnSubmit;
    Button btnClearForm;
    Button btnInsert;
    Button btnRemove;
    UserSQLiteHelper dbUserHelper;
    UserDataAccess da;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);
        app = (AppClass) getApplication();
        Music = (RadioGroup) findViewById(R.id.RadGroup);
        EditTxtEmail = (EditText) findViewById(R.id.EditTxtEmail);
        EditTxtName = (EditText) findViewById(R.id.EditTxtName);


        chkActive = (CheckBox)findViewById(R.id.chkActive);

        dbUserHelper = new UserSQLiteHelper(this);
        da = new UserDataAccess(dbUserHelper);


        btnSubmit = (Button)findViewById(R.id.btnSave);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(user!= null){
                    da.updateUser(getDataFromUI());
                    startActivity(new Intent(UserDetailsActivity.this, UserListActivity.class));
                }else {
                    Toast.makeText(UserDetailsActivity.this, "Cannot save a user that doesn't exist", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnRemove = (Button)findViewById(R.id.btnRemove);
        btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(user != null){
                    da.deleteUser(user.getId());
                    startActivity(new Intent(UserDetailsActivity.this, UserListActivity.class));
                }else{
                    Toast.makeText(UserDetailsActivity.this, "Cannot delete a user that doesn't exist", Toast.LENGTH_SHORT).show();
                }
                
            }
        });


        btnClearForm = (Button)findViewById(R.id.btnClear);
        btnClearForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetForm();
            }
        });
        btnInsert = (Button)findViewById(R.id.btnInsertData);
        btnInsert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if (passesValidation(getDataFromUI())){
                   da.insertUser(getDataFromUI());
                   startActivity(new Intent(UserDetailsActivity.this, UserListActivity.class));
               }else{
                   Toast.makeText(UserDetailsActivity.this, "Cannot insert a user that doesn't exist", Toast.LENGTH_SHORT).show();
               }
            }
        });

        Intent i = getIntent();
        long userId = i.getLongExtra(USER_ID_EXTRA, -1);
        if(userId >= 0){
            user =  app.getUserById(userId);
            putDataInUI();
            //Toast.makeText(this, "GET USER: " + userId, Toast.LENGTH_LONG).show();

        }
    }

    private boolean passesValidation(User u){
        //Toast.makeText(this, u.getFirstName(), Toast.LENGTH_LONG).show();
        boolean hasError = false;
        String msg = "";
        if(u.getFirstName()==null||u.getFirstName().equals("")) {
            //Toast.makeText(this, u.getFirstName(), Toast.LENGTH_LONG).show();
            msg+="It seems that you have forgotten to enter the user's name.\n";
            hasError=true;
        } if(u.getEmail()==null||u.getEmail().equals("")) {
            //Toast.makeText(this, "It seems that you have forgotten to enter the user's name.", Toast.LENGTH_LONG).show();
            msg+="It seems that you have forgotten to enter the user's email.\n";
            hasError=true;
        } if(u.getFavoriteMusic()==null){
            msg+="It seems you have not set the favorite music.\n";
            hasError=true;
        }
        if(!hasError)
            return true;
        else{
            Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
            return false;
        }
    }

    private void putDataInUI () {

        EditTxtName.setText(user.getFirstName());
        EditTxtEmail.setText(user.getEmail());
        chkActive.setChecked(user.isActive());

        switch (user.getFavoriteMusic()) {
            case COUNTRY:
                Music.check(R.id.radCountry);
                break;
            case JAZZ:
                Music.check(R.id.radJazz);
                break;
            case RAP:
                Music.check(R.id.radRap);
                break;
        }

    }


    private User getDataFromUI() {
        String firstName = EditTxtName.getText().toString();
        String Email = EditTxtEmail.getText().toString();
        boolean active = chkActive.isChecked();
        // Here's how we can set the favorite music...
        User.Music favoriteMusic = null;
        int selectedRadioButtonId = Music.getCheckedRadioButtonId();
        switch (selectedRadioButtonId) {
            case R.id.radCountry:
                favoriteMusic = User.Music.COUNTRY;
                break;
            case R.id.radJazz:
                favoriteMusic = User.Music.JAZZ;
                break;
            case R.id.radRap:
                favoriteMusic = User.Music.RAP;
                break;
        }
        if (user != null) {
            //user.setId(id);
            user.setFirstName(firstName);
            user.setEmail(Email);
            user.setFavoriteMusic(favoriteMusic);
            user.setActive(active);
            return user;
        } else {
            return  new User(1, firstName, Email, favoriteMusic, active);

        }
    }

    private void resetForm () {
        Music.clearCheck();
        EditTxtName.setText("");
        EditTxtEmail.setText("");
        chkActive.setChecked(false);
    }


}