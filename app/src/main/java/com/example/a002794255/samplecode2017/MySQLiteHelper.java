package com.example.a002794255.samplecode2017;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.a002794255.samplecode2017.dataaccess.ItemDataAccess;

import static android.R.attr.version;

/**
 * Created by 002794255 on 11/13/2017.
 */

public class MySQLiteHelper extends SQLiteOpenHelper {

    public static final String TAG = "MySQLiteHelper";
    public static final String DATA_BASE_NAME = "samples.db";
    public static final int DATABASE_VERSION = 1;

    public MySQLiteHelper(Context context) {
        super(context, DATA_BASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = ItemDataAccess.TABLE_CREATE;
        db.execSQL(sql);
        Log.d(TAG, "Creating DB: " + sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

}
