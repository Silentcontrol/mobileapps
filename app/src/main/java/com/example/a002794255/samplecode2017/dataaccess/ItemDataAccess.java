package com.example.a002794255.samplecode2017.dataaccess;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.example.a002794255.samplecode2017.MySQLiteHelper;
import com.example.a002794255.samplecode2017.models.models.Item;
import java.util.ArrayList;

/**
 * Created by 002794255 on 11/13/2017.
 */

public class ItemDataAccess {
    public static final String TAG = "ItemDataAccess";

     MySQLiteHelper dbHelper;
     SQLiteDatabase database;

    public static final String TABLE_NAME = "items";
    public static final String COLUM_ITEM_ID = "_id";
    public static final String  COLUM_ITEM_NAME = "item_name";

    public static final String TABLE_CREATE =
            String.format("create table %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s TEXT)",
            TABLE_NAME,
            COLUM_ITEM_ID,
            COLUM_ITEM_NAME

    );

    public ItemDataAccess(MySQLiteHelper dbHelper){
        this.dbHelper = dbHelper;
        this.database = this.dbHelper.getWritableDatabase();
    }


    public Item insertItem(Item i){
        ContentValues values = new ContentValues();
        values.put(COLUM_ITEM_NAME, i.getName());
        long insertId = database.insert(TABLE_NAME, null, values);
        i.setId(insertId);
        return i;
    }

    public ArrayList<Item> getAllItems(){

        ArrayList<Item> items = new ArrayList<>();
        String query = String.format("SELECT %s, %s FROM %s", COLUM_ITEM_ID, COLUM_ITEM_NAME, TABLE_NAME);

        Cursor c = database.rawQuery(query, null);
        c.moveToFirst();
        while(!c.isAfterLast()){
            Item i = new Item(c.getLong(0), c.getString(1));
            items.add(i);
            c.moveToNext();
        }
        c.close();
        return items;

    }

    public Item updateItem(Item i){
        ContentValues values = new ContentValues();
        values.put(COLUM_ITEM_ID, i.getId());
        values.put(COLUM_ITEM_NAME, i.getName());

        int rowsUpdated = database.update(TABLE_NAME, values, "_id = " + i.getId(), null);
        // this method returns the number of rows that were updated in the db
        // so that you could use it to confirm that your update worked
        return i;
    }

    public int deleteItem(Item i){
        int rowsDeleted = database.delete(TABLE_NAME, COLUM_ITEM_NAME + " = " + i.getId(), null);
        // the above method returns the number of row that were deleted
        return rowsDeleted;
    }




}
