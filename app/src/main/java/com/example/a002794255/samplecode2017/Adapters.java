package com.example.a002794255.samplecode2017;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.a002794255.samplecode2017.models.models.User;

import java.util.ArrayList;

public class Adapters extends AppCompatActivity {

    public static final String TAG = "Adapters";
    ListView listView;
    AppClass app;

    String[] names = {"Bob", "Sally", "Bettey"};
    ArrayList<User> users = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adapters);

        //This is the connection to AppClass and gives us access to all the global variables in AppClass
        app = (AppClass)getApplication();
        Toast.makeText(this, app.someGobalVariable, Toast.LENGTH_SHORT).show();


        listView = (ListView)findViewById(R.id.listView);

        users.add(new User(1,"Bob","bob@bob.com", User.Music.RAP, true));
        users.add(new User(2,"Sally","Sally@bob.com", User.Music.JAZZ, true));
        users.add(new User(3,"Betty","Betty@bob.com", User.Music.COUNTRY, true));

        example1(); //uses array of strings as the data source
        //example2(); //uses arraylist of users as the data source
        //example3();    //Users a custom item view in the users arraylist
    }
    public void example1(){

        ArrayList<String> userStrings = new ArrayList<>();
        for(User u : users){
            userStrings.add(u.getFirstName() + " " + u.getFavoriteMusic());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, userStrings);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            //This represents the list inside the adapter
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                User selectedUser = users.get(position);
                Toast.makeText(Adapters.this, selectedUser.toString(), Toast.LENGTH_LONG).show();

            }
        });
        /*
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, names);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
                                    //This represents the list inside the adapter
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(Adapters.this, "POS: " + position, Toast.LENGTH_LONG).show();
                String selectedString = names[position];
            }
        });
        */
    }
    public void example2(){
        ArrayAdapter<User> adapter = new ArrayAdapter<User>(this, android.R.layout.simple_list_item_1, users);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            //This represents the list inside the adapter
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                User selectedUser = users.get(position);
                Toast.makeText(Adapters.this, selectedUser.getFirstName(), Toast.LENGTH_LONG).show();
            }
        });

    }
    public void example3(){
        final ArrayAdapter<User> adapter = new ArrayAdapter<User>(this, R.layout.custom_user_list_item, R.id.lblFirstName, users){
            @Override
            public View getView(int position, View view, ViewGroup parent){
                View itemView = super.getView(position, view, parent);
                TextView lblFirstName = (TextView)itemView.findViewById(R.id.lblFirstName);
                CheckBox chkActive = (CheckBox)itemView.findViewById(R.id.chkActive);

                User u = users.get(position);
                lblFirstName.setText(u.getFirstName());
                chkActive.setChecked(u.isActive());

                chkActive.setTag(u);

                chkActive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        CheckBox pressedCheckedBox = (CheckBox) view;
                        User u = (User)pressedCheckedBox.getTag();
                        u.setActive(pressedCheckedBox.isChecked());
                        Toast.makeText(Adapters.this, u.toString(), Toast.LENGTH_LONG).show();
                    }
                });


                return itemView;
            }
        };
        listView.setAdapter(adapter);

    }


}
